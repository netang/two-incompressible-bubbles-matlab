function dy = Todd_system_rho_eq_0( t, y  )


global R01 R02 rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;

R1 = y(1);
dR1 = y(2);
U1 = y(3);
U2 = y(4);

r1 = y(5);
r2 = y(6);

d = abs(r2 - r1);

% 
% p_inf=-Ap*sin(omega*t);
% P1 = (Pg0*(R01/R1)^(3*gamma)-2*sigma/R1)-p_inf;

p_inf= 0;%-Ap*sin(omega*t);
%P1 = (Pg0*(R01/R1)^(3*gamma)-2*sigma/R1)-p_inf;


dy = zeros(6, 1);


P = [ -9*R01^3*R02^3*U2^2/d^4;  9*R01^3*R02^3*U1^2/d^4;];
M = [ R01^3                 -3*R01^3*R02^3/(d^3);
       -3*R01^3*R02^3/(d^3)           R02^3
    ];


dU = M\P;


dy(1) = dR1;
dy(2) = 0;
dy(3) = dU(1);
dy(4) = dU(2);


dy(5) = U1;
dy(6) = U2;





end

