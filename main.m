

global k R01 R02 ap d rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;
c = 1;
k = 10; %��� ������ k, ��� ������ ��������� ����� ��������� � ��������
R01 = 1e-5; % ������ ���������
R02 = c*(1e-5); % ������ �������
ap = R02;
d = R01+R02+k*R01; %��������� �� ������ �������� �� ������ �������
rho_l = 1000; % ��������� ��������
rho_p = 0;%2000; % ��������� �������, rho1 �� ����������, �.�. ������������ ��.
V02 = (4/3)*pi*R02^3; %�����  ������� (� ������ ������ �� �� ��������)
P0 = 1e+5; %����������� �������� (�������� � ��������)
gamma = 1.4; %����������� ���������
sigma = 0.073;
Pg0 = P0+2*sigma/R01;
Ap = P0;
nu = 200e+3;
omega = 2*pi*nu;
NT = 5;
tmax = NT/nu;
Nt=5001;
Toutput=linspace(0, tmax, Nt);
time = Toutput*nu;

options = odeset('RelTol',1e-4);%,'AbsTol',[1e-4 1e-4 1e-4 1e-4 1e-4 1e-4]);
[T1D,Y1D] = ode45(@Todd_system_rho_eq_0,Toutput,[R01 0 1 -1 0 d], options);%_d_dynamic
[T4,Y4] = ode45(@Gumerov_system_rho_eq_0, Toutput, [R01 0 1 -1 0 d], options);

saveflag = 0;
set(0,'DefaultAxesFontSize',14);

figure;
plot(time, abs(Y1D(:,3)),   time, abs(Y4(:,3)), 'LineWidth',2 );
legend('U = |U_1| = |U_2| Todd 1D (equation 9)',     'U = |U_1| = |U_2| N.A. 1D (equation 11)');
if saveflag ~= 0
    saveas(gcf,'fig8.png');
end;